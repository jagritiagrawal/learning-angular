'use strict';
(function() {
    angular
        .module('app', [])
        .factory('studentSvc', studentSvc);
    /* @ngInject */
    function studentSvc($q) {
        var service = {
            saveDetails: saveDetails,
            getAllStudents: getAllStudents
        };
        var studentArray = [];
        var count = 0;
        return service; ////////////////


        function saveDetails(obj) {
            if (Object.keys(obj).length > 0) {
                if (studentArray.length == 0) {
                    studentArray.push(obj);
                    return $q.when(obj)
                } else {
                    var i = 0;
                    var length = studentArray.length;
                    for(;i < length; i+=1) {
                        if (studentArray[i].email === obj.email) {
                            return $q.reject(obj);
                        }
                    };
                    studentArray.push(obj);
                    return $q.when(obj);
                }
            } else {
                return $q.reject(obj);
            }
        }

        function getAllStudents() {
            return studentArray;
        }
    }
})();
