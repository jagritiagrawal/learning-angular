'use strict';
(function() {
    angular
        .module('app')
        .controller('HomeCtrl', HomeCtrl);

    function HomeCtrl(studentSvc) {
        var vm = this;
        vm.ui = {};
        vm.add = add;
        vm.departments = {
            1: 'Electrical',
            2: 'EC'
        };
        vm.sections = {
            1: 'A',
            2: 'B'
        };
        vm.gender = {
            1: 'Male',
            2: 'Female',
            3: 'Others'
        };
        activate();

        function activate() {}

        function add() {
            return studentSvc.saveDetails(vm.ui).then(function(studentDetails) {
                vm.ui = {};
                vm.users = studentSvc.getAllStudents();
            });
        }
    }
})();
